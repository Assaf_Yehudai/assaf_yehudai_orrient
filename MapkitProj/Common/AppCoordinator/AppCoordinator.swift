//
//  AppCoordinator.swift
//  MapkitProj
//
//  Created by assaf yehudai on 12/02/2021.
//

import Foundation
import UIKit

class AppCoordinator: Coordinator {
    
    // MARK: - Coordinator Properties
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        
        let oriientMapViewModel = OriientMapViewModel(placesProvider: GooglePlacesManager(),
                                                      locationProvider: LocationProvider())
        let vc = OriientMapViewController(mapViweModel: oriientMapViewModel)
        
        navigationController.pushViewController(vc, animated: false)
    }
}

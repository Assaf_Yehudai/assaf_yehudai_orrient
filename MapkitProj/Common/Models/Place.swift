//
//  AnnotationObject.swift
//  MapkitProj
//
//  Created by assaf yehudai on 14/02/2021.
//

import Foundation
import CoreLocation

struct Place {
    
    let coordinate: CLLocationCoordinate2D
    let title: String?
    let subtitle: String?
}


//
//  CLLocation+ Ex.swift
//  MapkitProj
//
//  Created by assaf yehudai on 13/02/2021.
//

import Foundation
import CoreLocation

extension CLHeading {
    
    private func bottomHeadingRange(_ range: CLLocationDirection) -> CLLocationDirection {
        let bottom = magneticHeading - range/2
        return bottom < 0 ? bottom + 360.0 : bottom
    }
    
    private func topHeadingRange(_ range: CLLocationDirection) -> CLLocationDirection {
        let top = magneticHeading + range/2
        return top > 360.0 ? top - 360.0 : top
    }
    
    func isHeading(to direction: CLLocationDirection, in range: CLLocationDirection) -> Bool {
        
        let rangeBottom = bottomHeadingRange(range)
        let rangeTop = topHeadingRange(range)
        
        if rangeTop > rangeBottom {
            return rangeBottom <= direction && direction <= rangeTop
        }
        
        return  0 <= direction && direction <= rangeTop ||
                rangeBottom <= direction && direction <= 360.0
    }
}


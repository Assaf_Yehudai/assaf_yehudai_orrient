//
//  CLLocationCoordinate2D + Ex.swift
//  MapkitProj
//
//  Created by assaf yehudai on 14/02/2021.
//

import Foundation
import CoreLocation

extension CLLocationCoordinate2D {
    
    func relativeDirection(to coordinate: CLLocationCoordinate2D) -> CLLocationDirection {
    
        let deltaL = coordinate.longitude.toRadians - self.longitude.toRadians
        let thetaB = coordinate.latitude.toRadians
        let thetaA = self.latitude.toRadians
        let x = cos(thetaB) * sin(deltaL)
        let y = cos(thetaA) * sin(thetaB) - sin(thetaA) * cos(thetaB) * cos(deltaL)
        let bearing = atan2(x,y).toDegrees
        
        return bearing > 0 ? bearing : bearing + 360.0
    }
    
    func distance(from coordinate: CLLocationCoordinate2D) -> Double {
        let locA = CLLocation(latitude: latitude, longitude: longitude)
        let locB = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        return locA.distance(from: locB)
    }
 }

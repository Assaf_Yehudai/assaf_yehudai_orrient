//
//  PlacesProviderProtocol.swift
//  MapkitProj
//
//  Created by assaf yehudai on 12/02/2021.
//

import Foundation
import CoreLocation

enum PlacesProvidreResponse {
    case success(places:[Place])
    case fail(error: Error)
}

typealias PlacesPrividerCompletion = (PlacesProvidreResponse) -> ()

protocol PlacesProviderProtocol {
    
    func fetchPlaces(with userLocation: CLLocationCoordinate2D, amount:Int, completion: @escaping PlacesPrividerCompletion)
}


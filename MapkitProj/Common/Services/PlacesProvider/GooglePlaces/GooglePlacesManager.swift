//
//  GooglePlacesManager.swift
//  MapkitProj
//
//  Created by assaf yehudai on 12/02/2021.
//

import Foundation
import GooglePlaces

class GooglePlacesManager: PlacesProviderProtocol {
    
    // MARK: - Private Propeties
    private var placesClient = GMSPlacesClient.shared()
    
    
    // MARK: - Public Methods
    func fetchPlaces(with userLocation: CLLocationCoordinate2D, amount: Int, completion: @escaping PlacesPrividerCompletion) {
        
        let placeFiled: GMSPlaceField = [.name, .coordinate, .formattedAddress]
        
        placesClient.findPlaceLikelihoodsFromCurrentLocation(withPlaceFields: placeFiled) {(placeLikelihoods, error) in
            
            guard error == nil, let likehoods = placeLikelihoods else { completion(.fail(error: error!)); return }
            let places = likehoods.map({Place(coordinate: $0.place.coordinate, title: $0.place.name, subtitle: $0.place.formattedAddress)})
                                  .map{(place: $0, distance: $0.coordinate.distance(from: userLocation))}
                                  .sorted(by: {$0.distance < $1.distance})
                                  .prefix(amount).map{$0.place}

            completion(.success(places: places))
        }
    }
}

//
//  LocationProvider.swift
//  MapkitProj
//
//  Created by assaf yehudai on 14/02/2021.
//

import Foundation
import CoreLocation

class LocationProvider: NSObject, LocationProviderProtocol {
    
    // MARK: - Constants
    let k_distanceToPlaceRefresh = 20.0
    let k_headingFillter = 10.0
    
    // MARK: - Delegate
    weak var delegate: LocationProviderDelegate?
    
    // MARK: - Private Properties
    private var locationManager: CLLocationManager!
    private var lastLocation: CLLocation!
    
    // MARK: - Constructor
    override init() {
        super.init()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.headingFilter = k_headingFillter
        locationManager.distanceFilter = k_distanceToPlaceRefresh
        
        lastLocation = locationManager.location
    }
    
    // MARK: - Public Method
    func startListeningForUpdates() {
        locationManager.startUpdatingHeading()
        locationManager.startUpdatingLocation()
    }
}

// MARK: - CLLocationManagerDelegate
extension LocationProvider: CLLocationManagerDelegate {
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        
        switch manager.authorizationStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            guard let location = manager.location else {
                delegate?.errorAccured(error: .failToGetUserLocation)
                return
            }
            delegate?.locationPermissionApproved(userLocation: location)
        
        case .denied, .restricted:
            delegate?.errorAccured(error: .permissionNeeded)
        case .notDetermined: break
        default:
            print("---- Unknown authorizationStatus: \(manager.authorizationStatus) ----")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.first else {return}
        if let last = lastLocation, last.distance(from: location) > k_distanceToPlaceRefresh {
            lastLocation = location
            delegate?.didUpdateLocation(newLocation: location)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        guard let userCoordinate = manager.location?.coordinate else { return }
        delegate?.didUpdateHeading(newHeading: newHeading, userCoordinates: userCoordinate)
    }
}

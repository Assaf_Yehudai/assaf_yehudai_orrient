//
//  LocationProviderProtocol.swift
//  MapkitProj
//
//  Created by assaf yehudai on 14/02/2021.
//

import Foundation
import CoreLocation

protocol LocationProviderProtocol {
    var delegate: LocationProviderDelegate? {set get}
    func startListeningForUpdates()
}

protocol LocationProviderDelegate: class {
    
    func didUpdateHeading(newHeading: CLHeading, userCoordinates: CLLocationCoordinate2D)
    func didUpdateLocation(newLocation: CLLocation)
    func locationPermissionApproved(userLocation: CLLocation)
    func errorAccured(error: LocationError)
}

enum LocationError: Error {
    case permissionNeeded
    case failToGetUserLocation
}

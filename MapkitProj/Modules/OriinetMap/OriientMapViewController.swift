//
//  OriientMapViewController.swift
//  MapkitProj
//
//  Created by assaf yehudai on 14/02/2021.
//

import UIKit

class OriientMapViewController: UIViewController {

    // MARK: - Private Propeties
    private var mapComponentViewModel: OriientMapViewModel?
    
    // MARK: - IBOutlets
    @IBOutlet weak var mapView: OriientMapView!
    
    // MARK: - Constructor
    init(mapViweModel: OriientMapViewModel) {
        
        super.init(nibName: "OriientMapViewController", bundle: nil)
        mapComponentViewModel = mapViweModel
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.setup(with: mapComponentViewModel)
        mapView.alertDelegate = self
    }
}

extension OriientMapViewController: OriientMapViewAlertDelegate {
    
    func presentAlert(_ alert: UIAlertController) {
        present(alert, animated: true)
    }
}

//
//  Alerts.swift
//  MapkitProj
//
//  Created by assaf yehudai on 15/02/2021.
//

import Foundation
import UIKit

func retryAlert( retryBlock: @escaping () -> ()) -> UIAlertController {
    
    let alert = UIAlertController(title: "Error Accured",
                                  message: "Please retry",
                                  preferredStyle: .alert)
    
    let action = UIAlertAction(title: "Retry", style: .default) {[retryBlock] _ in
        retryBlock()
    }
    let cancel = UIAlertAction(title: "Cancel", style: .default)
    
    alert.addAction(action)
    alert.addAction(cancel)
    
    return alert
}

func permissionAlert() -> UIAlertController {
    
    let alert = UIAlertController(title: "Location Permission Required",
                                            message: "Please enable location permissions in settings.",
                                            preferredStyle: .alert)
    
    let okAction = UIAlertAction(title: "Settings",style: .default) { _ in
        DispatchQueue.main.async {
            guard let url = URL(string:UIApplication.openSettingsURLString) else { return }
            UIApplication.shared.open(url)
        }
    }
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
    
    alert.addAction(cancelAction)
    alert.addAction(okAction)
    
    return alert
}

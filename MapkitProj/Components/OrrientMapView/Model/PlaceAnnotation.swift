//
//  PlaceAnnotation.swift
//  MapkitProj
//
//  Created by assaf yehudai on 15/02/2021.
//

import Foundation
import MapKit

class PlaceAnnotation: NSObject, MKAnnotation {
    
    let coordinate: CLLocationCoordinate2D
    let title: String?
    let subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
}

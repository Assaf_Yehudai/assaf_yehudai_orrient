//
//  AnnotationsFactory.swift
//  MapkitProj
//
//  Created by assaf yehudai on 15/02/2021.
//

import Foundation
import MapKit


class AnnotaionsFactory {
    
    static func createAnnotations(from places: [Place]) -> [PlaceAnnotation] {
        return places.map{PlaceAnnotation(coordinate: $0.coordinate, title: $0.title, subtitle: $0.subtitle)}
    }
}

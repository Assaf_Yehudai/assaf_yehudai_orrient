//
//  OrrientMapView.swift
//  MapkitProj
//
//  Created by assaf yehudai on 14/02/2021.
//

import UIKit
import MapKit

let k_annotation_idetifier = "annotaion_identifier"
let k_amount = 10

protocol OriientMapViewAlertDelegate where Self: UIViewController {
    func presentAlert(_ alert: UIAlertController )
}

class OriientMapView: MKMapView {

    // MARK: - Alert Delegate
    weak var alertDelegate: OriientMapViewAlertDelegate?
    
    // MARK: - Private Properties
    private var viewModel: OriientMapViewModel?
    
    // MARK: - Public Methods
    func setup(with viewModel: OriientMapViewModel?) {
        
        // set viewModel
        self.viewModel = viewModel
        self.viewModel?.delegate = self
        
        delegate = self
        showsUserLocation = true
        
        register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: k_annotation_idetifier)
    }
}

// MARK: - OriientMapViewModelDelegate
extension OriientMapView: OriientMapViewModelDelegate {

    func add(_ annotations: [MKAnnotation]) {
        addAnnotations(annotations)
    }
    
    func remove(_ annotations: [MKAnnotation]) {
        removeAnnotations(annotations)
    }
    
    func updateNewHeading(with diffArray: [AnnotaionDiff]) {
        
        for diff in diffArray {
            let view = self.view(for: diff.annotation)
            view?.alpha = diff.isInRange ? 1 : 0.3
        }
        setUserTrackingMode(.followWithHeading, animated: true)
    }
    
    func alertPerrmissionNeeded() {
        alertDelegate?.presentAlert(permissionAlert())
    }
    
    func alertRetryFetchingPlaces() {
        alertDelegate?.presentAlert(retryAlert() { [weak self] in
            guard let location = self?.userLocation.coordinate else {return}
            self?.viewModel?.fetchPlaces(with: location)
        })
    }
}

// MARK: - MKMapViewDelegate
extension OriientMapView: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard !annotation.isKind(of: MKUserLocation.self) else { return nil }
        let view = mapView.dequeueReusableAnnotationView(withIdentifier: k_annotation_idetifier, for: annotation)
        (view as? MKMarkerAnnotationView)?.markerTintColor = .orange
        (view as? MKMarkerAnnotationView)?.animatesWhenAdded = true
        view.alpha = 0.3
        return view
    }
}



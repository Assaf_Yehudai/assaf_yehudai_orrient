//
//  OriientMapViewModel.swift
//  MapkitProj
//
//  Created by assaf yehudai on 14/02/2021.
//

import Foundation
import MapKit

typealias AnnotaionDiff = (annotation: MKAnnotation, isInRange: Bool)

protocol OriientMapViewModelDelegate: class {
    func add(_ annotations: [MKAnnotation])
    func remove(_ annotations: [MKAnnotation])
    func updateNewHeading(with diffArray: [AnnotaionDiff])
    func alertPerrmissionNeeded()
    func alertRetryFetchingPlaces()
}

class OriientMapViewModel {
    
    // MARK: - Constant
    let k_highlightRange = 45.0
    
    // MARK: - Proerties
    weak var delegate: OriientMapViewModelDelegate?
    private var placesProvider: PlacesProviderProtocol!
    private var locationProvider: LocationProviderProtocol!
    
    // MARK: - Model
    private var annotations: [MKAnnotation]? {
        willSet {
            if let currentAnnotations = annotations {
                delegate?.remove(currentAnnotations)
            }
        }
        didSet {
            if let newAnnotations = annotations {
                delegate?.add(newAnnotations)
            }
        }
    }
    
    // MARK: - Constructor
    init(placesProvider: PlacesProviderProtocol, locationProvider: LocationProviderProtocol) {
        
        self.locationProvider = locationProvider
        self.locationProvider.delegate = self
        self.locationProvider.startListeningForUpdates()
        
        self.placesProvider = placesProvider
    }
    
    // MARK: - Public Methods
    func fetchPlaces(with userLocation: CLLocationCoordinate2D) {
        /*
            In general ViewModels wouldn't make network requests, and would ask for Data from
            Repository / DataManager that handles app state and persistance if needed.
         */
        placesProvider.fetchPlaces(with: userLocation, amount: k_amount) {[weak self] resposne in
            
            switch resposne {
            
            case .success(let places):
                let annotations = AnnotaionsFactory.createAnnotations(from: places)
                DispatchQueue.main.async {
                    self?.annotations = annotations
                }
                
            case .fail(let error):
                print("Current place error: \(error.localizedDescription)")
                DispatchQueue.main.async {
                    self?.delegate?.alertRetryFetchingPlaces()
                }
            }
        }
    }
}

// MARK: - LocationProviderDelegate
extension OriientMapViewModel: LocationProviderDelegate {
        
    func locationPermissionApproved(userLocation: CLLocation) {
        fetchPlaces(with: userLocation.coordinate)
    }
    
    func didUpdateHeading(newHeading: CLHeading, userCoordinates: CLLocationCoordinate2D) {
        
        guard let annotations = self.annotations else {return}
        let diffArray: [AnnotaionDiff] = annotations.map { (annotaion) -> AnnotaionDiff in
            let direction = userCoordinates.relativeDirection(to: annotaion.coordinate)
            let isInRange = newHeading.isHeading(to: direction, in: k_highlightRange)
            return (annotation: annotaion, isInRange: isInRange)
        }
        delegate?.updateNewHeading(with: diffArray)
    }
    
    func didUpdateLocation(newLocation: CLLocation) {
        fetchPlaces(with: newLocation.coordinate)
    }
    
    func errorAccured(error: LocationError) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            switch error {
            case .failToGetUserLocation:
                self?.delegate?.alertRetryFetchingPlaces()
            case .permissionNeeded:
                self?.delegate?.alertPerrmissionNeeded()
            }
        }
    }
}
